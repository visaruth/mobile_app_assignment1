package com.example.megas.assignment1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {
//    นายวิศรุต พันธ์เนียม 5709611528
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        EditText name = (EditText) findViewById(R.id.editText);
        String nameBy = getIntent().getStringExtra("name");
        name.setText(nameBy);

        EditText lname = (EditText) findViewById(R.id.editText2);
        String lnameBy = getIntent().getStringExtra("lname");
        lname.setText(lnameBy);

        EditText age = (EditText) findViewById(R.id.editText3);
        String ageBy = getIntent().getStringExtra("age");
        age.setText(ageBy);

        ImageView profile = (ImageView)findViewById(R.id.imageView);

        if(Integer.parseInt(ageBy)>=0&&Integer.parseInt(ageBy)<=15)profile.setImageResource(R.drawable.kid);
        if(Integer.parseInt(ageBy)>=16&&Integer.parseInt(ageBy)<=25)profile.setImageResource(R.drawable.teen);
        if(Integer.parseInt(ageBy)>=26&&Integer.parseInt(ageBy)<=60)profile.setImageResource(R.drawable.work);
        if(Integer.parseInt(ageBy)>=61&&Integer.parseInt(ageBy)<=150)profile.setImageResource(R.drawable.old);

        EditText email = (EditText) findViewById(R.id.editText4);
        String emailBy = getIntent().getStringExtra("email");
        email.setText(emailBy);

        EditText pnum = (EditText) findViewById(R.id.editText5);
        String pnumBy = getIntent().getStringExtra("pnum");
        if(pnumBy.length()==9){
            pnumBy = ""+pnumBy.charAt(0)+pnumBy.charAt(1)+"-"+pnumBy.charAt(2)+pnumBy.charAt(3)+pnumBy.charAt(4)+"-"+pnumBy.charAt(5)+pnumBy.charAt(6)
                    +pnumBy.charAt(7)+pnumBy.charAt(8);
        }else if(pnumBy.length()==10){
            pnumBy = ""+pnumBy.charAt(0)+pnumBy.charAt(1)+pnumBy.charAt(2)+"-"+pnumBy.charAt(3)+pnumBy.charAt(4)+pnumBy.charAt(5)+"-"+pnumBy.charAt(6)
                    +pnumBy.charAt(7)+pnumBy.charAt(8)+pnumBy.charAt(9);
        }
        pnum.setText(pnumBy);
    }
}
