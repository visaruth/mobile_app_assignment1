package com.example.megas.assignment1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
//    นายวิศรุต พันธ์เนียม 5709611528
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button summit = (Button)findViewById(R.id.button);
    }

    public void submitData(View v){
        boolean check = true;
        Intent nextActivity = new Intent(MainActivity.this, MainActivity2.class);
        String name = (String)((EditText)findViewById(R.id.editText)).getText().toString();
        String lname = (String)((EditText)findViewById(R.id.editText2)).getText().toString();
        String age = (String)((EditText)findViewById(R.id.editText3)).getText().toString();
        String email = (String)((EditText)findViewById(R.id.editText4)).getText().toString();
        String pnum = (String)((EditText)findViewById(R.id.editText5)).getText().toString();

        nextActivity.putExtra("name", name);
        nextActivity.putExtra("lname", lname);
        nextActivity.putExtra("age", age);
        nextActivity.putExtra("email", email);
        nextActivity.putExtra("pnum", pnum);

        if(name.equalsIgnoreCase("")||lname.equalsIgnoreCase("")||age.equalsIgnoreCase("")||email.equalsIgnoreCase("")||pnum.equalsIgnoreCase("")){
            Toast.makeText(MainActivity.this,"Please complete all editText.", Toast.LENGTH_SHORT).show();
            check = false;
        }

        if(!name.matches("[A-Z]+[a-z]*")&&check){
            Toast.makeText(MainActivity.this,"The first character of name is upper case.", Toast.LENGTH_SHORT).show();
            check = false;
        }

        if(!lname.matches("[A-Z]+[a-z]*")&&check){
            Toast.makeText(MainActivity.this,"The first character of lastname is upper case.", Toast.LENGTH_SHORT).show();
            check = false;
        }

        if(!email.matches("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)")&&check){
            Toast.makeText(MainActivity.this,"Invalid email.", Toast.LENGTH_SHORT).show();
            check = false;
        }

        if(!(pnum.length()==10||pnum.length()==9)){
            Toast.makeText(MainActivity.this,"The telephone number should have 10 or 9 numbers.", Toast.LENGTH_SHORT).show();
            check = false;
        }

        if(check)startActivity(nextActivity);
    }


}
